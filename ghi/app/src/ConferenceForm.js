import React, { Component } from "react";

class ConferenceForm extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            name: '',
            starts: '',
            ends: '',
            description: '',
            max_presentations: '',
            max_attendees: '',
            locations: [],
        }
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }
    async componentDidMount() {
        const url = 'http://localhost:8000/api/locations/';
    
        const response = await fetch(url);
    
        if (response.ok) {
          const data = await response.json();
          this.setState({locations: data.locations});
    
        }
      }
      handleChange(event) {
        const value = event.target.value;
        const key = event.target.name
        const changeDict = {}
        changeDict[key] = value
        this.setState(changeDict);
    }

    async handleSubmit(event) {
        event.preventDefault();
        const data = {...this.state};
        delete data.locations
        console.log(data);

        const conferenceUrl = 'http://localhost:8000/api/conferences/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
            'Content-Type': 'application/json',
            },
        };

        const response = await fetch(conferenceUrl, fetchConfig);
        if (response.ok) {
            const newConference = await response.json();
            console.log(newConference);
        
            const cleared = {
                name: '',
                starts: '',
                ends: '',
                description: '',
                max_presentations: '',
                max_attendees: '',
                location: '',
            };
            this.setState(cleared);
          }
    }
    render () {
        return (
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>Create a new conference</h1>
                        <form onSubmit={this.handleSubmit} id="create-conference-form">
                            <div className="form-floating mb-3">
                                <input onChange={this.handleChange} value={this.state.name} placeholder="Name" required type="text" name='name' id="name" className="form-control" />
                                <label htmlFor="name">Name</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input placeholder="Starts" onChange={this.handleChange} value={this.state.starts} required type="date" name='starts' id="starts" className="form-control" />
                                <label htmlFor="starts">Starts</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input placeholder="Ends" onChange={this.handleChange} value={this.state.ends}required type="date" name='ends' id="ends" className="form-control" />
                                <label htmlFor="ends">Ends</label>
                            </div>
                            <div className="form-floating mb-3">
                                <textarea placeholder="Description" onChange={this.handleChange} value={this.state.description} required type="text" name='description' id="description" className="form-control" rows="4"></textarea>
                                <label htmlFor="description">Description</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input placeholder="max_presentations" onChange={this.handleChange} value={this.state.max_presentations} required type="number" name='max_presentations' id="max_presentations" className="form-control" />
                                <label htmlFor="max_presentations">Max presentations</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input placeholder="max_attendees" onChange={this.handleChange} value={this.state.max_attendees} required type="number" name='max_attendees' id="max_attendees" className="form-control" />
                                <label htmlFor="max_attendees">Max attendees</label>
                            </div>
                            <div className="mb-3">
                                <select required onChange={this.handleChange} value={this.state.location} name='location' id="location" className="form-select">
                                <option value="">Choose a location</option>
                                {this.state.locations.map(location => {
                                        return (
                                            <option key={location.id} value={location.id}>
                                                {location.name}
                                            </option>
                                        )
                                    })}
                                </select>
                            </div>
                            <button className="btn btn-primary">Create</button>
                        </form>
                    </div>
                </div>
            </div>
        )
    }
}

export default ConferenceForm