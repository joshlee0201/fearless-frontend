import React, { Component } from "react";

class PresentationForm extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            presenter_name: '',
            presenter_email: '',
            company_name: '',
            synopsis: '',
            title: '',
            conferences: [],
        }
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }
    async componentDidMount() {
        const url = 'http://localhost:8000/api/conferences/';
    
        const response = await fetch(url);
    
        if (response.ok) {
          const data = await response.json();
          this.setState({conferences: data.conferences});
    
        }
      }
      handleChange(event) {
        const value = event.target.value;
        const key = event.target.name
        const changeDict = {}
        changeDict[key] = value
        this.setState(changeDict);
    }

    async handleSubmit(event) {
        event.preventDefault();
        const data = {...this.state};
        delete data.conferences
        console.log(data);

        const conferenceId = data['conference']
        const presentationUrl = `http://localhost:8000/api/conference/${conferenceId}/presentations`;
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
            'Content-Type': 'application/json',
            },
        };

        const response = await fetch(presentationUrl, fetchConfig);
        if (response.ok) {
            const newConference = await response.json();
            console.log(newConference);
        
            const cleared = {
                presenter_name: '',
                presenter_email: '',
                company_name: '',
                synopsis: '',
                title: '',
                conference: '',
            };
            this.setState(cleared);
          }
    }
    render () {
        return (
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>Create a new presentation</h1>
                        <form onSubmit={this.handleSubmit} id="create-presentation-form">
                            <div className="form-floating mb-3">
                                <input onChange={this.handleChange} value={this.state.presenter_name} 
                                    placeholder="Presenter name" required type="text" 
                                    name='presenter_name' id="presenter_name" className="form-control" />
                                <label htmlFor="presenter_name">Presenter name</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input placeholder="Presenter email" onChange={this.handleChange} 
                                    required type="email" name='email' id="email" className="form-control" />
                                <label htmlFor="presenter_email">Presenter email</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input placeholder="Company name" onChange={this.handleChange} 
                                    value={this.state.company_name}required type="text" 
                                    name='company_name' id="company_name" className="form-control" />
                                <label htmlFor="company_name">Company name</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input placeholder="Title" onChange={this.handleChange} 
                                    value={this.state.title}required type="text" 
                                    name='title' id="title" className="form-control" />
                                <label htmlFor="title">Title</label>
                            </div>
                            <div className="form-floating mb-3">
                                <textarea placeholder="Synopsis" onChange={this.handleChange} 
                                    value={this.state.synopsis} required type="text" 
                                    name='synopsis' id="synopsis" className="form-control" rows="4">
                                </textarea>
                                <label htmlFor="synopsis">Synopsis</label>
                            </div>
                            <div className="mb-3">
                                <select required onChange={this.handleChange} value={this.state.conference} name='conference' id="conference" className="form-select">
                                <option value="">Choose a conference</option>
                                {this.state.conferences.map(conference => {
                                        return (
                                            <option key={conference.name} value={conference.id}>
                                                {conference.name}
                                            </option>
                                        )
                                    })}
                                </select>
                            </div>
                            <button className="btn btn-primary">Create</button>
                        </form>
                    </div>
                </div>
            </div>
        )
    }
}

export default PresentationForm